
stage:
	rsync -rav -e ssh . dream.public.cat:staging/ --delete

publish:
	rsync -rav -e ssh . dream.public.cat:production/ --delete

.PHONY: stage publish
