<!--
SPDX-FileCopyrightText: 2021 hellekin
SPDX-FileCopyrightText: 2021 petites singularités

SPDX-License-Identifier: LAL-1.3
-->

# dream.public.cat

This repository contains the home page visible at https://dream.public.cat

## Usage

Pushing to the `main` branch deploys to https://dream-stage.public.cat/.

Pushing a [SemVer] tag deploys to https://dream.public.cat/.

### SSH Deployment

You can use `make stage` and `make publish` respectively to deploy to
`https://dream-stage.public.cat` and `https://dream.public.cat`. 

This method uses SSH. You should have something like this in your SSH configuration:

```config
Host dream.public.cat
     HostName           159.69.150.145
     CheckHostIP        yes
     Port               2222
     User               dream
     IdentityFile       ~/.ssh/id_ed25519
     IdentitiesOnly     yes
     LogLevel           info
```

### Web Hook

E.g., `curl -i -X POST https://$CREDENTIALS@dream.public.cat/api/deploy/production/v0.1.0`

In `dream.public.cat` Nginx configuration:

```nginx
        location /api {
                auth_basic 'DREAM API';
                # htpasswd -c -B -C 9 dream.public.cat.access deploy
                #                    v 0700  v 0600
                auth_basic_user_file secrets/dream.public.cat.access;
        }

        # LUA
        location ~ ^/api/deploy/(stage|production)(/?(v.+))*$ {
                default_type 'text/plain';

                set $to  $1;
                set $tag $3;

                content_by_lua_file /etc/nginx/scripts/dream-api.lua;
        }
```

See `dream-api.lua` for the listener.

## License

Copyright © 2020-2021 petites singularités

Code is AGPL-3.0.

Content is LAL-1.3.

See LICENSES directory.

