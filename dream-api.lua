-- SPDX-FileCopyrightText: 2021 hellekin
-- SPDX-FileCopyrightText: 2021 petites singularités
-- SPDX-License-Identifier: AGPL-3.0-or-later


--[[ dream-api.lua ]]

-- Require POST method to avoid search spider bots
if ngx.var.request_method ~= "POST" then
        ngx.say('Try POST')
        ngx.exit(ngx.OK)
end

-- By default deploy the main branch to stage
tag = "main"
to  = ngx.var.to
deploy_dir = "/srv/www/public.cat/dream-stage"

-- Git repository
upstream_repo = 'https://gitlab.com/public.dream/dream.public.cat.git/'
bare_repo     = '/srv/www/public.cat/dream.git'

-- Fetch refs
fetch_refs    = 'origin HEAD:refs/heads/main'

-- In production require an existing Git tag to deploy
if to == 'production' then
        tag = ngx.var.tag .. '' -- git tag to deploy

        -- TODO Check whether it's a known tag (and exit)

        -- Check that the tag exists before deploying (or exit)
        upsteam_tag = 'https://gitlab.com/public.dream/dream.public.cat/-/tags/' .. tag

        -- Fetch the tag ref
        fetch_refs  = 'origin refs/tags/' .. tag .. ':refs/tags/' .. tag

        -- Save tag to avoid deploying it again
        deploy_dir = deploy_dir.gsub(deploy_dir, '-stage', '') -- deploy to production

end

ngx.say('Deploying ', tag, ' to ', to)

local os = require 'os'

os.execute('cd /srv/www/public.cat')

-- Checkout the site
cmd_clone = 'test -f ' .. bare_repo .. '/config || ( umask 022 && git clone --bare -- ' .. upstream_repo .. ' ' .. bare_repo .. ' ) '
ngx.say(cmd_clone)
os.execute(cmd_clone)
os.execute('/usr/bin/find ' .. bare_repo .. ' -type d -exec chmod 0700 {} \\;')
os.execute('/usr/bin/find ' .. bare_repo .. ' -type f -exec chmod 0600 {} \\;')

-- Prepare deployment directory
cmd_deploy_dir = 'mkdir -m 0755 -p ' .. deploy_dir
ngx.say(cmd_deploy_dir)
os.execute('mkdir -m 0755 -p ' .. deploy_dir)

-- Deploy the repository
cmd_fetch = 'git --git-dir ' .. bare_repo .. ' fetch '
cmd_deploy = cmd_fetch .. fetch_refs .. ' && ' .. cmd_fetch .. ' -t && cd ' .. deploy_dir .. ' && git --git-dir ' .. bare_repo .. ' --work-tree ' .. deploy_dir .. ' checkout -f ' .. tag
--cmd_deploy = cmd_fetch .. ' -t && cd ' .. deploy_dir .. ' && git --git-dir ' .. bare_repo .. ' --work-tree ' .. deploy_dir .. ' checkout -f ' .. tag
ngx.say(cmd_deploy)
os.execute(cmd_deploy)

-- Fix permissions
os.execute('/usr/bin/find ' .. deploy_dir .. ' -type d -exec chmod 0755 {} \\;')
os.execute('/usr/bin/find ' .. deploy_dir .. ' -type f -exec chmod 0644 {} \\;')

ngx.exit(ngx.OK)
